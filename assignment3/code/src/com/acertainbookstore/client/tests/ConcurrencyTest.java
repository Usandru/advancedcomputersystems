package com.acertainbookstore.client.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.acertainbookstore.business.Book;
import com.acertainbookstore.business.BookCopy;
import com.acertainbookstore.business.SingleLockConcurrentCertainBookStore;
import com.acertainbookstore.business.ImmutableStockBook;
import com.acertainbookstore.business.StockBook;
import com.acertainbookstore.business.TwoLevelLockingConcurrentCertainBookStore;
import com.acertainbookstore.client.BookStoreHTTPProxy;
import com.acertainbookstore.client.StockManagerHTTPProxy;
import com.acertainbookstore.interfaces.BookStore;
import com.acertainbookstore.interfaces.StockManager;
import com.acertainbookstore.utils.BookStoreConstants;
import com.acertainbookstore.utils.BookStoreException;

public class ConcurrencyTest {

    /** The Constant TEST_ISBN. */
    private static final int TEST_ISBN = 3044560;

    /** The Constant NUM_COPIES. */
    private static final int NUM_COPIES = 500;

    /** The local test. */
    private static boolean localTest = true;

    /** Single lock test */
    private static boolean singleLock = false;


    /** The store manager. */
    private static StockManager storeManager;

    /** The client. */
    private static BookStore client;

    /**
     * Sets the up before class.
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        try {
            String localTestProperty = System.getProperty(BookStoreConstants.PROPERTY_KEY_LOCAL_TEST);
            localTest = (localTestProperty != null) ? Boolean.parseBoolean(localTestProperty) : localTest;

            String singleLockProperty = System.getProperty(BookStoreConstants.PROPERTY_KEY_SINGLE_LOCK);
            singleLock = (singleLockProperty != null) ? Boolean.parseBoolean(singleLockProperty) : singleLock;

            if (localTest) {
                if (singleLock) {
                    SingleLockConcurrentCertainBookStore store = new SingleLockConcurrentCertainBookStore();
                    storeManager = store;
                    client = store;
                } else {
                    TwoLevelLockingConcurrentCertainBookStore store = new TwoLevelLockingConcurrentCertainBookStore();
                    storeManager = store;
                    client = store;
                }
            } else {
                storeManager = new StockManagerHTTPProxy("http://localhost:8081/stock");
                client = new BookStoreHTTPProxy("http://localhost:8081");
            }

            storeManager.removeAllBooks();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Helper method to add some books.
     *
     * @param isbn
     *            the isbn
     * @param copies
     *            the copies
     * @throws BookStoreException
     *             the book store exception
     */
    public void addBooks(int isbn, int copies) throws BookStoreException {
        Set<StockBook> booksToAdd = new HashSet<StockBook>();
        StockBook book = new ImmutableStockBook(isbn, "Test of Thrones", "George RR Testin'", (float) 10, copies, 0, 0,
                0, false);
        booksToAdd.add(book);
        storeManager.addBooks(booksToAdd);
    }

    /**
     * Helper method to get the default book used by initializeBooks.
     *
     * @return the default book
     */
    public StockBook getDefaultBook() {
        return new ImmutableStockBook(TEST_ISBN, "Harry Potter and JUnit", "JK Unit", (float) 10, NUM_COPIES, 0, 0, 0,
                false);
    }

    /**
     * Method to add a book, executed before every test case is run.
     *
     * @throws BookStoreException
     *             the book store exception
     */
    @Before
    public void initializeBooks() throws BookStoreException {
        Set<StockBook> booksToAdd = new HashSet<StockBook>();
        booksToAdd.add(getDefaultBook());
        storeManager.addBooks(booksToAdd);
    }

    /**
     * Method to clean up the book store, execute after every test case is run.
     *
     * @throws BookStoreException
     *             the book store exception
     */
    @After
    public void cleanupBooks() throws BookStoreException {
        storeManager.removeAllBooks();
    }

    //Custom utility functions start here

    public void buyBooksLoop(Set<BookCopy> bookCopiesToBuy, int n) {
        for (int i = 0; i < n; i++) {
            try {
                client.buyBooks(bookCopiesToBuy);
            } catch (Exception ex){
                ;
            }
        }
    }

    public void stockBooksLoop(Set<BookCopy> bookCopiesSet, int n) {
        for (int i = 0; i < n; i++) {
            try {
                storeManager.addCopies(bookCopiesSet);
            } catch (Exception ex){
                ;
            }
        }
    }

    public List<Thread> spawnLoopingThreads(Set<BookCopy> bookSet, int n) {
        Runnable buyer =
                () -> { buyBooksLoop(bookSet, n);};

        Runnable stocker =
                () -> { stockBooksLoop(bookSet, n);};

        Thread buyThread = new Thread(buyer);
        Thread stockThread = new Thread(stocker);

        buyThread.start();
        stockThread.start();

        List<Thread> threadList = new ArrayList<>();
        threadList.add(buyThread);
        threadList.add(stockThread);

        return threadList;
    }

    public List<Thread> spawnSeparateSetThreads(Set<BookCopy> bookSet1, Set<BookCopy> bookSet2, int n) {
        Runnable buyer =
                () -> { buyBooksLoop(bookSet1, n);};

        Runnable stocker =
                () -> { stockBooksLoop(bookSet2, n);};

        Thread buyThread = new Thread(buyer);
        Thread stockThread = new Thread(stocker);

        buyThread.start();
        stockThread.start();

        List<Thread> threadList = new ArrayList<>();
        threadList.add(buyThread);
        threadList.add(stockThread);

        return threadList;
    }

    public void buyThenStockLoop(Set<BookCopy> bookCopies, int n) {
        for (int i = 0; i < n; i++) {
            try {
                client.buyBooks(bookCopies);
                storeManager.addCopies(bookCopies);
            } catch (Exception ex){
                ;
            }
        }
    }

    //Tests start here

    /**
     * Tests atomicity with concurrent function calls.
     *
     * @throws BookStoreException
     *             the book store exception
     */
    @Test
    public void testAtomicity() throws BookStoreException {
        // Set of books to buy
        Set<BookCopy> bookSet = new HashSet<BookCopy>();
        bookSet.add(new BookCopy(TEST_ISBN, 1));

        List<Thread> threads = spawnLoopingThreads(bookSet, NUM_COPIES);

        Thread c1 = threads.remove(0);
        Thread c2 = threads.remove(0);

        while(c1.isAlive() || c2.isAlive()){
            continue;
        }

        List<StockBook> listBooks = storeManager.getBooks();
        assertTrue(listBooks.size() == 1);
        StockBook bookInList = listBooks.get(0);

        assertTrue(bookInList.getNumCopies() == NUM_COPIES);
    }

    /**
     * Tests database consistency with concurrent function calls.
     *
     * @throws BookStoreException
     *             the book store exception
     */
    @Test
    public void testConsistency() throws BookStoreException {
        // Set of books to buy
        int NUM_OF_LOOPS = 2000;

        Set<BookCopy> bookSet = new HashSet<BookCopy>();
        bookSet.add(new BookCopy(TEST_ISBN, NUM_COPIES));

        Runnable buyStockLoop =
                () -> { buyThenStockLoop(bookSet, NUM_OF_LOOPS);};

        Thread c1 = new Thread(buyStockLoop);

        c1.start();

        while(c1.isAlive()){
            try {
                Set<Integer> testISBNList = new HashSet<Integer>();
                testISBNList.add(TEST_ISBN);
                List<StockBook> listBooks = storeManager.getBooksByISBN(testISBNList);
                StockBook bookInList = listBooks.get(0);

                assertTrue((bookInList.getNumCopies() == NUM_COPIES) || (bookInList.getNumCopies() == 0));
            } catch (Exception ex) {
                fail();
            }
        }
    }

    /**
     * Tests for deadlocks
     *
     * @throws BookStoreException
     *             the book store exception
     */
    @Test
    public void testDeadlocks() throws BookStoreException {
        // Set of books to buy
        Set<BookCopy> bookSet1 = new HashSet<BookCopy>();
        Set<BookCopy> bookSet2 = new HashSet<BookCopy>();

        int NUM_OF_BOOKS = 1000;

        for (int i = 1; i < NUM_OF_BOOKS; i++){
            addBooks(i, 5);
            BookCopy newBook = new BookCopy(i, 1);
            bookSet1.add(newBook);
            bookSet2.add(newBook);
        }

        List<Thread> threads = spawnSeparateSetThreads(bookSet1, bookSet2, 5);

        Thread c1 = threads.remove(0);
        Thread c2 = threads.remove(0);

        while(c1.isAlive() || c2.isAlive()){
            continue;
        }

    }

    /**
     * Tests that bad requests don't crash the system
     *
     * @throws BookStoreException
     *             the book store exception
     */
    @Test
    public void testBadRequest() throws BookStoreException {
        // Set of books to buy
        Set<BookCopy> bookSet = new HashSet<BookCopy>();
        Set<BookCopy> badSet = new HashSet<BookCopy>();
        bookSet.add(new BookCopy(TEST_ISBN, 1));
        badSet.add(new BookCopy(-1, 1));


        List<Thread> threads = spawnLoopingThreads(bookSet, 5);

        Thread c1 = threads.remove(0);
        Thread c2 = threads.remove(0);

        while(c1.isAlive() || c2.isAlive()){
            spawnLoopingThreads(badSet, 5);
        }

        List<StockBook> listBooks = storeManager.getBooks();
        assertTrue(listBooks.size() == 1);
        StockBook bookInList = listBooks.get(0);

        assertTrue(bookInList.getNumCopies() == NUM_COPIES);
    }

    //Tests end here

    /**
     * Tear down after class.
     *
     * @throws BookStoreException
     *             the book store exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws BookStoreException {
        storeManager.removeAllBooks();

        if (!localTest) {
            ((BookStoreHTTPProxy) client).stop();
            ((StockManagerHTTPProxy) storeManager).stop();
        }
    }

}
